package QDTS;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for Exercise7.
 */

@DisplayName("7th exercise Test")
public class OnlineMediaTest {
    private final static PipedOutputStream pipe = new PipedOutputStream();
    private final static ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final static ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final static InputStream originalIn = System.in;
    private final static PrintStream originalOut = System.out;
    private final static PrintStream originalErr = System.err;

    @BeforeAll
    public static void catchIO() throws IOException {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        System.setIn(new PipedInputStream(pipe));
    }

    @AfterAll
    public static void releaseIO() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalIn);
    }


    @Test
    @DisplayName("7th exercise Test")
    public void OnlineMediaTestMain() throws Exception {

        OnlineMedia.main(new String[0]);
        String[] output = out.toString().split(System.lineSeparator());
        assertEquals("dvd1.title = IBM Dance Party", output[0]);
        assertEquals("Playing DVD: IBM Dance Party", output[1]);
        assertEquals("DVD length: 87", output[2]);
        assertEquals("dvd2.title = IBM Kids Sing-along", output[3]);
        assertEquals("Playing DVD: IBM Kids Sing-along", output[4]);
        assertEquals("DVD length: 124", output[5]);
        assertEquals("dvd3.title = IBM Smarter Planet", output[6]);
        assertEquals("Playing DVD: IBM Smarter Planet", output[7]);
        assertEquals("DVD length: 90", output[8]);
        assertEquals("Book title = Java Programming", output[9]);
        assertEquals("cd1.title = IBM Symphony", output[10]);
        assertEquals("Cost of CD = 29.95", output[11]);
        //assertEquals("------------------------------------", output[12]);
        assertEquals("The tracks in NOT sorted order are: ", output[13]);
        assertEquals("t1.title = Warmup", output[14]);
        assertEquals("t2.title = Scales", output[15]);
        assertEquals("t3.title = Introduction", output[16]);
        assertEquals("Playing CD: IBM Symphony", output[17]);
        assertEquals("CD length:10", output[18]);
        assertEquals("Playing track: Warmup", output[19]);
        assertEquals("Track length: 3", output[20]);
        assertEquals("Playing track: Scales", output[21]);
        assertEquals("Track length: 4", output[22]);
        assertEquals("Playing track: Introduction", output[23]);
        assertEquals("Track length: 3", output[24]);
        //assertEquals("------------------------------------", output[25]);
        assertEquals("The tracks in sorted order are: ", output[26]);
        assertEquals("Introduction", output[27]);
        assertEquals("Scales", output[28]);
        assertEquals("Warmup", output[29]);
        //assertEquals("-----------------------------------", output[30]);
        assertEquals("The total length of the CD is 10", output[31]);
        assertEquals("Total Cost of the Order is: 163.83", output[32]);
    }
}
